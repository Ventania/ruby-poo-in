require_relative 'classes.rb'

a = Person.new 'Lucas', 'xxx.xxx.xxx-xx', '20','11/01/1999', 'Rua Kemal Pacha, n42'

a.present do |name, cpf, age, birth, address|
		puts "Meu nome e #{name}, inscrito sob o cpf #{cpf}, minha idade e de #{age} anos. Nasci em #{birth}, e meu endereco e #{address}\n\n"
	end


b = Student.new 'Lucas', 'xxx.xxx.xxx-xx', '20','11/01/1999', 'Rua Kemal Pacha, n42', '999.999.999'

b.present do |name, cpf, age, birth, address, matricula|
		puts "Meu nome e #{name}, inscrito sob o cpf #{cpf}, minha idade e de #{age} anos. Nasci em #{birth}, e meu endereco e #{address}, e minha matricula e #{matricula}\n\n"
	end

puts 'to estudando?'
puts b.to_estudando?

puts 'me locomovi'
b.locomote

puts 'to estudando?'
puts b.to_estudando?

puts 'me locomovi'
b.locomote

puts 'to estudando?'
puts b.to_estudando?

p = Prof.new 'Lucas', 'xxx.xxx.xxx-xx', '20','11/01/1999', 'Rua Kemal Pacha, n42'

p.present do |name, cpf, age, birth, address, formacoes, materias|
	puts "Meu nome e #{name}, inscrito sob o cpf #{cpf}, minha idade e de #{age} anos. Nasci em #{birth}, e meu endereco e #{address}. Sou formado em #{formacoes.join(', ')}, e dou aula das seguintes materias: #{materias.join(', ')}\n\n"
	end

